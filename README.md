# anz_webAutomation
I tried my best to automate using BDD - Cucumber framework( Self Learned in short time)
Steps To Follow to execute the test cases:
1. Once you download/clone the project in your local system.
2. Install Maven: https://maven.apache.org/install.html
3. Download the chrome driver for your Current Chrome browser version: https://chromedriver.chromium.org/downloads 
4. extract the chromedriver and copy.
5. Now go to anz/src/test/resources folder and paste/replace chromedriver executable file.
6. Now in terminal navigate project folder "anz" where the pom.xml file present.
7. Run command: mvn test
8. Now you can see all the 3 test cases executing on chrome browser.

Note: I developed this code in mac, it was running successfully without any failures.
`
