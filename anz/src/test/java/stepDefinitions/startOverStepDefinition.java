package stepDefinitions;

import config.SeleniumConfig;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageFactory.MuchBorrowPage;

public class startOverStepDefinition extends SeleniumConfig{
	MuchBorrowPage mp ;
	@Given("borrow page with some values filled")
	public void borrow_page_with_some_values_filled() {
	  System.out.println(" We are in Much Borrow page of the ANZ application with some values filled!");
	    
	}

	@When("I click startOver button")
	public void i_click_start_over_button(){
		mp = new MuchBorrowPage(driver); 
		mp.clickStartOver();
	    
	}

	@Then("borrow estimate form input fields values must reset")
	public void borrow_estimate_form_input_fields_values_must_reset() {
		mp = new MuchBorrowPage(driver); 
		boolean isStartOverWorkingFine = mp.isStartOverButtonFeatureWorkingFine();
		if(isStartOverWorkingFine == true)
			System.out.println(" On click of StartOver button, has succesfully reset the input fields values");
		else
			System.out.println(" StartOver button is not functioning properly!");
	}
}
