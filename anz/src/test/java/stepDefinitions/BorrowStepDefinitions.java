package stepDefinitions;


import org.testng.Assert;
import config.SeleniumConfig;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageFactory.MuchBorrowPage;


public class BorrowStepDefinitions extends SeleniumConfig{
	MuchBorrowPage mp;
	@Given("borrow page url")
	public void borrow_page_url() {
		launchBrowser();
		String actualPageTitle=driver.getTitle();
		Assert.assertEquals(actualPageTitle, pr.getExpectedTitleOfBorrowCalculatorPage());
		System.out.println(" We are in Much Borrow page of the ANZ Application");
		
	}

	@When("click {string} button")
	public void click_button(String string) throws InterruptedException {
		
		mp = new MuchBorrowPage(driver);
		   mp.selectYourApplicationType(pr.getApplicationType());
		   mp.selectNumberOfDependants(pr.getNumberOfDependants());
		   mp.selectPropertyToBuy(pr.getPropertyToBuy());
		   mp.enterAnnualIncomeBeforeTax(pr.getAnnualIncomeBeforeTax());
		   mp.enterOptionalAnnualOtherIncome(pr.getAnnualOtherIncome());
		   mp.enterMonthlyLivingExpenses(pr.getMonthlyLivingExpenses());
		   mp.enterCurrentHomeLoanMonthlyRepayment(pr.getCurrentMonthlyHomeLoanRepayment());
		   mp.enterOtherLoanMonthlyRepayment(pr.getOtherLoanMonthlyRepayment());
		   mp.enterOtherMonthlyCommitments(pr.getOtherMonthlyCommitment());
		   mp.enterTotalCreditCardLimits(pr.getTotalCreditCardLimits());
		   mp.clickToCalculateBorrowEstimation();	   
	}

	@Then("I should get expected borrow estimate")
	public void i_should_get_expected_borrow_estimate() throws InterruptedException {
		mp = new MuchBorrowPage(driver);	
		String estimatedBorrowAmount = mp.getResultedAmountOfBorrowEstimation();
		System.out.println(" Estimated Borrow Amount is: "+estimatedBorrowAmount);
		Assert.assertEquals(estimatedBorrowAmount, pr.getExpectedBorrowEstimateAmount());		
	}
}
