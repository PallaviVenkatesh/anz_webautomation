package stepDefinitions;

import org.testng.Assert;
import config.SeleniumConfig;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageFactory.MuchBorrowPage;

public class UnableToCalculateBorrowEstimationStepDefinitions extends SeleniumConfig{
	MuchBorrowPage mp;
	@Given("Borrow estimate page")
	public void borrow_estimate_page() {
		String actualPageTitle=driver.getTitle();
		Assert.assertEquals(actualPageTitle, pr.getExpectedTitleOfBorrowCalculatorPage());
		System.out.println(" We are in Much Borrow page of the ANZ Application");
	}

	@When("Enter ${int} for living expenses and click ‘Work out how much I could borrow’ button")
	public void enter_$_for_living_expenses_and_click_work_out_how_much_i_could_borrow_button(Integer int1) throws InterruptedException {
		mp = new MuchBorrowPage(driver);
		   mp.enterMonthlyLivingExpenses(pr.getMonthlyLivingExpenses2());
		   mp.clickToCalculateBorrowEstimation();
	}

	@Then("I validate the triggered message to user")
	public void i_validate_the_message_triggered_to_user() throws InterruptedException {
		String actualErrorMessage = mp.borrowErrorMessageText();
		String expectedErrorMessage = pr.getExpectedBorrowErrorMessage();
		Assert.assertTrue(actualErrorMessage.contains(expectedErrorMessage));
		System.out.println(" Expected borrow error message triggered successfully to user");
		System.out.println(" "+actualErrorMessage);
		quitBrowser();
	}
}
