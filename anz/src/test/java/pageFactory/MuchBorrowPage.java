package pageFactory;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class MuchBorrowPage {
	WebDriver driver;
	@FindBy(xpath = "//label[@for='application_type_single']")
	private WebElement applicationTypeSingle_Button;
	@FindBy(xpath = "//label[@for='application_type_joint']")
	private WebElement applicationTypeJointButton;
	@FindBy(xpath = "//select[@title='Number of dependants']")
	private WebElement numberOfDependantsDropdown;
	@FindBy(xpath = "//label[contains(text(), 'Home to live in')]")
	private WebElement homeToLiveInButton;
	@FindBy(xpath = "//label[contains(text(), 'Residential investment')]")
	private WebElement residentialInvestmentButton;    
	@FindBy(xpath = "//*[label[contains(text(), 'Your annual income (before tax)')]]//input")
	private WebElement annualIncomeBeforeTaxTextField;
	@FindBy(xpath = "//*[label[contains(text(), 'Your annual other income (optional)')]]//input")
	private WebElement annualOtherIncomeTextField;
	@FindBy(xpath = "//*[label[contains(text(), 'Monthly living expenses ')]]//input")
	private WebElement monthlyLivingExpensesTextField;
	@FindBy(xpath = "//*[label[contains(text(), 'Current home loan monthly')]]//input")
	private WebElement currentHomeLoanMonthlyRepaymentTextField;
	@FindBy(xpath = "//*[label[contains(text(), 'Other loan monthly repayments')]]//input")
	private WebElement otherLoanMonthlyRepaymentTextField;
	@FindBy(xpath = "//*[label[contains(text(), 'Other monthly commitments')]]//input")
	private WebElement otherMonthlyCommitmentTextField;
	@FindBy(xpath = "//*[label[contains(text(), 'Total credit card limits')]]//input")
	private WebElement totalCreditcardLimitsTextField;
	@FindBy(xpath = "//button[contains(text(), ' Work out how much I could borro')]")
	private WebElement calculateBorrowEstimateButton;
	@FindBy(id = "borrowResultTextAmount")
	private WebElement borrowResultAmountText;
	@FindBy(xpath = "//div[@class='result__restart']//button")
	private WebElement startOverButton;
	@FindBy(className ="borrow__error__text")
	private WebElement borrowErrorMessageText;
	
	public MuchBorrowPage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
		
	}
	
	public void selectYourApplicationType(String applicationType) {
		if(applicationType.equalsIgnoreCase("single")){
			applicationTypeSingle_Button.click();
		}else {
			applicationTypeJointButton.click();
		}
	}
	public void selectNumberOfDependants(String numberOfDependants) {
		Select select = new Select(numberOfDependantsDropdown);
		select.selectByVisibleText(numberOfDependants);
	}
	public void selectPropertyToBuy(String propertyToBuy) {
		if(propertyToBuy.equalsIgnoreCase("home")){
			homeToLiveInButton.click();
		}else {
			residentialInvestmentButton.click();
		}
	}
	public void enterAnnualIncomeBeforeTax(String annualIncomeBeforeTax) {
		annualIncomeBeforeTaxTextField.sendKeys(annualIncomeBeforeTax);
	}
	public void enterOptionalAnnualOtherIncome(String annualOtherIncome) {
		annualOtherIncomeTextField.sendKeys(annualOtherIncome);
	}
	public void enterMonthlyLivingExpenses(String monthlyLivingExpenses) {
		monthlyLivingExpensesTextField.sendKeys(monthlyLivingExpenses);	
	}
	public void enterCurrentHomeLoanMonthlyRepayment(String currentHomeLoanMonthlyrepayment) {
		currentHomeLoanMonthlyRepaymentTextField.sendKeys(currentHomeLoanMonthlyrepayment);
	}
	public void enterOtherLoanMonthlyRepayment(String otherLoanMonthlyRepayment) {
		otherLoanMonthlyRepaymentTextField.sendKeys(otherLoanMonthlyRepayment);
	}
	public void enterOtherMonthlyCommitments(String otherMonthlyCommitments) {
		otherMonthlyCommitmentTextField.sendKeys(otherMonthlyCommitments);	
	}
	public void enterTotalCreditCardLimits(String totalCreditCardLimits) {
		totalCreditcardLimitsTextField.sendKeys(totalCreditCardLimits);
	}
	public void clickToCalculateBorrowEstimation() throws InterruptedException {
		calculateBorrowEstimateButton.click();
		Thread.sleep(5000);
	}
	public String getResultedAmountOfBorrowEstimation() {
		return borrowResultAmountText.getText();
	}
	public void clickStartOver() {		
		startOverButton.click();
	}
	public boolean isStartOverButtonFeatureWorkingFine() {
		String annualIncomeBeforeTaxAfterReset=annualIncomeBeforeTaxTextField.getAttribute("value");
		String estimatedBorrowAmount = borrowResultAmountText.getText();
		if(annualIncomeBeforeTaxAfterReset.equals("0") && estimatedBorrowAmount.equals("$0")) {
			return true;
		}
		else {
			return false;
		}
	}	
	public String borrowErrorMessageText() throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", borrowErrorMessageText);
		return borrowErrorMessageText.getText();
	}
}
