package config;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import utilityFiles.PropertyFileReader;

public class SeleniumConfig {
	public static WebDriver driver;
	public static PropertyFileReader pr;

public static void launchBrowser() {

	pr = new PropertyFileReader();
	String url = pr.getApplicationURL();
	
	System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
	driver=new ChromeDriver();
	driver.manage().window().maximize();
	//driver.get("https://www.anz.com.au/personal/home-loans/calculators-tools/much-borrow/");
	driver.get(url);
	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

}

public static void quitBrowser() {
	driver.close();
	}
}
