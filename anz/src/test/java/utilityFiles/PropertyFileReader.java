package utilityFiles;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Properties;

public class PropertyFileReader {
	private Properties properties;
	String propertyFilePath="src/test/resources/propertyFiles/config.properties";
	public PropertyFileReader() {
	try {
	BufferedReader bufReader = new BufferedReader(new FileReader(propertyFilePath));
	properties = new Properties();
	properties.load(bufReader);
	bufReader.close();
	}catch(Exception e){
		e.printStackTrace();
	}
	}
	public String getApplicationURL() {
		return properties.getProperty("applicationURL");
	}
	public String getExpectedTitleOfBorrowCalculatorPage() {
		return properties.getProperty("expectedCalculateBorrowPageTitle"); 
	}
	public String getApplicationType() {
		return properties.getProperty("applicationType");
	}
	public String getNumberOfDependants() {
		return properties.getProperty("numberOfDependants");
	}
	public String getPropertyToBuy() {
		return properties.getProperty("propertyToBuy");
	}
	public String getAnnualIncomeBeforeTax() {
		return properties.getProperty("annualIncomeBeforeTax");
	}
	public String getAnnualOtherIncome() {
		return properties.getProperty("annualOtherIncome");
	}
	public String getMonthlyLivingExpenses() {
		return properties.getProperty("monthlyLivingExpenses");
	}
	public String getCurrentMonthlyHomeLoanRepayment() {
		return properties.getProperty("currentHomeLoanMonthlyRepayment");
	}
	public String getOtherLoanMonthlyRepayment() {
		return properties.getProperty("otherLoanMonthlyRepayment");
	}
	public String getOtherMonthlyCommitment() {
		return properties.getProperty("otherMonthlyCommitment");
	}
	public String getTotalCreditCardLimits() {
		return properties.getProperty("totalCreditCardLimits");
	}
	public String getExpectedBorrowEstimateAmount() {
		return properties.getProperty("expectedBorrowEstimateAmount");
	}
	public String getMonthlyLivingExpenses2() {
		return properties.getProperty("monthlyLivingExpenses2");
	}
	public String getExpectedBorrowErrorMessage() {
		return properties.getProperty("expectedBorrowErrorMessageText");
	}
}
