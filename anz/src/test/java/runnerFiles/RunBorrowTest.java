package runnerFiles;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions(features = "src/test/resources/featureFiles/muchBorrow.feature", glue = "stepDefinitions")
public class RunBorrowTest extends AbstractTestNGCucumberTests{

}
