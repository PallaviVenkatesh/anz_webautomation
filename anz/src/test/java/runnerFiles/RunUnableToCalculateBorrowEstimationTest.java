package runnerFiles;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "src/test/resources/featureFiles/unableToCalculateBorrowEstimation.feature", glue = "stepDefinitions")
public class RunUnableToCalculateBorrowEstimationTest extends AbstractTestNGCucumberTests{

}
