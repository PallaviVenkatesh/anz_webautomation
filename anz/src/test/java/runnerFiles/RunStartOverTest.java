package runnerFiles;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "src/test/resources/featureFiles/startOver.feature", glue = "stepDefinitions")
public class RunStartOverTest extends AbstractTestNGCucumberTests{

}
